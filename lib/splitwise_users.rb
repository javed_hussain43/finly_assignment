require_relative 'user'

class SplitwiseUsers
	
	@@users = []

	class << self
    
    def find_or_create_by_id(id)
    	user = find_user(id)
    	unless user
    		user = create_user(id)
    	end
    	user
    end

    def find_or_create_by_ids(ids)
    	split_users = []
    	ids.each do |id|
    		user = find_user(id)
	    	if user
	    		split_users.push(user)
	    	else
	    		split_users.push(create_user(id))
	    	end
    	end
    	split_users
    end

    def users
    	@@users
    end

    private

    def find_user(id)
    	@@users.select do |user|
    		user.id == id
    	end.first
    end

    def create_user(id)
    	user = User.new(id: id)
    	@@users.push(user)
    	user
    end

	end
	
	
end