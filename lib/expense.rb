require 'securerandom'
require_relative 'splitwise_users'
require_relative 'expenses/equal_split'

class Expense
	
	def initialize(args)
		length = args.size
		@id = SecureRandom.hex
		@paid_by = SplitwiseUsers.find_or_create_by_id(args[1])
		@amount = args[2].to_i
		@no_of_splits = args[3].to_i
		@split_type = args.last
		@splits = SplitwiseUsers.find_or_create_by_ids(args[4..length-2])
	end

	def call
		case split_type
		when 'EQUAL'
			EqualSplit.new(
				paid: paid_by, 
				amount: amount,
				no_of_splits: no_of_splits,
				splits: splits
				).call
		when 'EXACT'
		when 'PERCENT'
		when 'SHARE'
		else
			"Invalid split_type"
		end		
	end

	attr_accessor :id, :paid_by, :amount, :no_of_splits, :split_type, :splits
	
	
end