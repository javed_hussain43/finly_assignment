require 'securerandom'

class Transaction
	
	def initialize(args)
		@id       = SecureRandom.hex
		@amount   = args[:amount]
		@owe_user = args[:user]
		@type     = args[:type]
	end

	def debit?
		self.type == 'debit'
	end

	def credit?
		self.type == 'credit'
	end

	attr_accessor :id, :amount, :owe_user, :type
	
	
end