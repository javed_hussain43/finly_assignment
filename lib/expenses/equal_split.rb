require_relative 'transaction'

class EqualSplit
	
	def initialize(args)
		@user = args[:paid]
		@amount = args[:amount]
		@no_of_splits = args[:no_of_splits]
		@splits = args[:splits]
	end

	def call
		amount_to_be_paid = (@amount / @no_of_splits).to_f
		@splits.each do |split|
			if split.id != @user.id
				@user.transactions.push(
					Transaction.new(
						type: 'credit',
						amount: amount_to_be_paid,
						user: split
						)
					)
				split.transactions.push(
					Transaction.new(
						type: 'debit',
						amount: amount_to_be_paid,
						user: @user
						)
					)
			end
		end
		@user.print_transactions
	end
	
	
end