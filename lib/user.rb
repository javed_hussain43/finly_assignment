require 'securerandom'

class User
	
	def initialize(id:)
		@id = id
		@transactions = []
	end

	def print_transactions
		self.transactions.map do |transaction|
			"#{transaction.owe_user.id} owes #{self.id} :  #{transaction.amount}"
		end
	end

	def debit_transactions
		find_all_transactions
	end

	def credit_transactions
		find_all_transactions('credit?')
	end

	def total_owe_amount
		credit_transactions.map(&:amount).sum - debit_transactions.map(&:amount).sum
	end


	attr_accessor :id, :transactions

	private

	def find_all_transactions(type='debit?')
		self.transactions.select do |transaction|
			transaction.send(type)
		end
	end
	
	
end